class Dataset:
    def __init__(self):
        """
            Constructor to initialize variables.
        """
        self.__columns = ()
        self.__records = []
        self.__lines = None
        self.__rows = 0
    
    def __createColumnHeaders(self):
        """
            private function to create headers tuple from read file. 
            This function assumes first line is header file and stores
            in object's variable.
        """
        if len(self.__lines) >= 1:
            columnstr = self.__lines[0]
            self.__columns = tuple(columnstr.split())
            
    def __createRecords(self):
        """
            Private function creates records, except header row and store row as tuple in list.
        """
        if len(self.__lines) < 1:
            raise Exception("No records in file.")
        # slice after 1st elemet in the list and store it in list  after
        # dividing it in columns.
        for line in self.__lines[1:]:
            if len(line.strip()) < 1:
                continue
            self.__records.append(tuple(line.split()))
            
    def __validateColumnsRecords(self):
        """
            Private function used to validate number of elements in each row
            is equal to number of elements of header row.
        """ 
        for record in self.__records:
            if len(record) != len(self.__columns):
                raise Exception("Column and record length mismatch.")
            for value in record:
                if value is None or value == "":
                    del record
            
            
    def readTrainingDataset(self, filename=""):
        """
            This function is invoke to load training dataset, then it invokes private
            functions to create headers and records, and finally validates data.
             
            This function accepts filename as the input parameter. It opens
            the file and read the contents.
            
            Parameter:
                filename: (string) accepts filename to read the dataset
        """
        try:
            # open file in read mode
            dbfile = open("data/" + filename)
            # read all content of the file, each row as string and store in list
            self.__lines = dbfile.readlines()
            # call to private function to create column header  
            self.__createColumnHeaders()
            # call to private function to create rows of data except header.
            self.__createRecords()
            # validation
            self.__validateColumnsRecords()
            # save count of rows in dataset to avoid additional computation
            self.__rows = len(self.__records)
        except IOError:
            print("File not found!")
        except Exception:
            print("Error! Unexpected error occurred while loading dataset.")
    
    def getEligibleTargetClass(self,binary):
        """
            This function finds candidate attributes having binary values.
            
            Return:
                target_class: (list) returns the list of attributes.
        """
        target_class = []
        for i in range(len(self.__columns)):
            distinct = set()
            for record in self.__records:
                distinct.add(record[i])
            
            if len(distinct) == 2 and binary == 1:
                target_class.append(self.__columns[i])
            elif binary !=1:
                target_class.append(self.__columns[i])
        return target_class
    
    def writeRuleToFile(self, filename, rules):
        """
            Invoke this function to write rule to output stream. Output is 
            stored under data folder of project root.
            
            Parameter:
                filename: (string) output filename
                rules: (string) output buffer rules to be written
        """
        
        try:
            writefile = open("data/" + filename, 'w')
            if writefile is None:
                raise Exception("File cannot be opened.")
            writefile.writelines(rules)
            writefile.close()
        except Exception as exp:
            print(str(exp) + "Error occurred while writing rules.")
        
    
    def getRecords(self):
        return self.__records
    
    def getRowsCount(self):
        return self.__rows
    
    def getColumnHeaders(self):
        return self.__columns
    
