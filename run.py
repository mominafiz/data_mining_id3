# script file to run the algorithm.
from dataset import Dataset
from ui import userInterface, targetClass
from algorithm import ID3

# create dataset object.
dbObj = Dataset()
userinput_tuple = userInterface()
#userinput_tuple=('data1','data1.rules5')
# loads training dataset from given filename
dbObj.readTrainingDataset(userinput_tuple[0])
# get eligible target classes
targetclass = dbObj.getEligibleTargetClass(userinput_tuple[2])
# calls function to ask user to select target class with binary variable
targetclass = targetClass(targetclass)
#targetclass = 'Humidity'
# convert tuple to columns
columns = list(dbObj.getColumnHeaders())
# get index of target class
target_index = columns.index(targetclass)

# create object 
algoObj = ID3(targetclass)
algoObj.headers = dbObj.getColumnHeaders()
# build decision tree
tree = algoObj.buildTree(dbObj.getRecords(),columns,target_index)
# generate rules
algoObj.generateRules(tree, dbObj.getColumnHeaders())
# write rules to output file given by user.
dbObj.writeRuleToFile(userinput_tuple[1], algoObj.output_string)

# print results to console.
print("\nTree data structure:")
print tree
print("\n")
print("Rules:")
print(algoObj.output_string)
print("\n")

print("Output written to file. Please check file under data folder of project root.")
print("------------------    End of the Program    -------------------------------")

algoObj.output_string = ""