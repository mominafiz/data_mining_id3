# implement UI for data file input, target attribute.

def userInterface():
    """
        This function is the implementation for User Interface.
        User enters input file name and output filename.
        
        Return:
            (tuple): filename and output filename as tuple
    """
    filename = raw_input("Enter filename of training data: ")
    if filename == None or filename == "":
        raise ValueError("Invalid filename.")

    output_filename = raw_input("Enter output file name: ")
    if output_filename == None:
        raise ValueError("Invalid filename.")
    
    binary = raw_input("Enter 1 to select binary variable class else 0: ")
    try:
        binary = int(binary)
    except:
        raise ValueError("Incorrect value for binary variable")
    return filename, output_filename,binary

def targetClass(target_class_list):
    """
        This function asks user to input target class from the list
        of attributes.
        
        Parameter:
            target_class_list: (list) list of target class.
        
        Return:
            (string): target attribute 
    """
    for i in range(len(target_class_list)):
        print("{0}    {1}".format(i+1,target_class_list[i]))
    target = raw_input("Enter target # of class/attribute: ")
    try:
        if target is None or target == "":
            raise ValueError("Invalid target class/attribute.")
        target = int(target)
        if target < 1 and target > len(target_class_list):
            raise ValueError("Please enter integer for target class in given range.")
    except Exception as exp:
        print(exp)
        targetClass(target_class_list)
    return target_class_list[target-1]