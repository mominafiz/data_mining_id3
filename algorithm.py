
import math
import operator
 
class ID3:
    def __init__(self, targetAtt):
        """
            Constructor of the class that initializes the variables.
            
            Parameter:
                targetAtt: (string) target attribute 
        """
        self.targetAtt = targetAtt
        self.headers = []
        self.rootnode_str = "\tIf {0} is {1}, then "
        self.leafnode_str = "{0} is {1}.\n"
        self.output_string = ""
        
    
     
    def entropy(self, dataset, colIndex):
        """
            This function calculates the entropy for given dataset.
            
            Paramter:
                dataset: (list of list) 
                
            Return:
                entro: (float) entropy of each attribute.
        """
        
        # get size of record
        records = len(dataset)
        # store occurence of unique key value pairs
        labels = {}
        for record in dataset:
            value = record[colIndex]
            if value not in labels.keys():
                labels[value] = 0
            labels[value] += 1
        entro = 0.0
        # calculate the entropy 
        for key in labels.keys():
            prob = float(labels[key]) / records
            logval = math.log(prob, 2)
            entro += -prob * logval
            
        return entro
     
    def splitDataset(self, dataset, noColumns, value):
        """
            This function splits dataset for purity
            
            Parameters:
                dataset: records in the file
                noColumns: (int) index of the column for which data has to be split
                value: (string) input on which split has to be created.
                
            Paraemter:
                returnSet: (list) subset of dataset 
        """
        returnSet = []
        for record in dataset:
            if record[noColumns] == value:
                #tempRec = list(record[:noColumns])
                #tempRec.extend(list(record[noColumns + 1:]))
                returnSet.append(list(record))
        return returnSet
     
    def selectBestFeature(self, dataset, colIndex):
        """
            This function returns best attribute as root at each level for DT.
            It calculates entropy for target class and then identifies the
            root attribute by splitting the data and calculating entropy for each
            subset and comparing with information gain.
            
            Parameter:
                dataset: (list) records from user input file
            
            Return:
                best_feature: (int) index of attribute in dataset.
                
        """
        # calculate entropy for target attributed
        base_entropy = self.entropy(dataset, colIndex)
        info_gain = 0.0
        best_feature = -1
        
        #sel_attr_index = self.headers.index(self.targetAtt)
        # find best attribute as root element.
        for i in range(len(dataset[0])):
            # list of values for ith attribute of dataset
            featureList = [x[i] for x in dataset]
            # store unique values in set
            uniqueSet = set(featureList)
            newEntropy = 0.0
            if colIndex == i:
                continue
#             if sel_attr_index > i: 
#                 colIndex = sel_attr_index - 1 
#             else:
#                 colIndex = sel_attr_index
            # aggregate entropy values over split
            for value in uniqueSet:
                # split dataset depending on value in each dataset for ith attribute
                subset = self.splitDataset(dataset, i, value)
                # calculate entropy
                prob = len(subset) / float(len(dataset))
                newEntropy += prob * self.entropy(subset, colIndex)
            # calculate info gain
            infoGain = base_entropy - newEntropy
            # if greater than zero then select attribute as best option
            if infoGain > info_gain:
                info_gain = infoGain
                best_feature = i
        return best_feature
     
    def buildTree(self, dataset, labels, sel_attr_index):
        """
            It is the main function that creates decision tree. It recursively 
            adds root nodes at each level of DT and base out at leaf node.
            
            Parameter:
                dataset: (list) records in user input file.
                labels: (list) columns in dataset.
                
            Return:
                tree: (complex structure) returns the DT (check console for structure)
        """
        # get all values of user selected attribute and store in list
        attr_list = [ x[sel_attr_index] for x in dataset]
        # if occurrence of first element in the list is equal to total list
        if attr_list.count(attr_list[0]) == len(attr_list):
            return attr_list[0]
        
        # find the best attribute at ith level of DT
        bestAttr = self.selectBestFeature(dataset, sel_attr_index)
            
        bestAttrLabel = self.headers[bestAttr]
        #bestAttrLabel = labels[bestAttr]
        tree = {bestAttrLabel:{}}
        # relocate user target column after best attribute is found.
        # it is required because recursively length of th subset is reduced.
#         if sel_attr_index > bestAttr: 
#             sel_attr_index -= 1 
        # delete column for which root is identified
        
        #del(labels[labels.index(bestAttrLabel)])

        if bestAttr == -1:
            return attr_list[0]
        # get list of values for selected attribute
        bestAttrValues = [x[bestAttr] for x in dataset]
        # create set of unique values 
        uniqueVals = set(bestAttrValues)
        # for each values in unique set expand the tree further recursively.
        for value in uniqueVals:
            splitDS = self.splitDataset(dataset, bestAttr, value)
            tree[bestAttrLabel][value] = self.buildTree(splitDS, labels, sel_attr_index)
        
        return tree
    
    def generateRules(self, tree, columns, idepth=-1, root=None):
        """
            Generates the rules from tree
            
            Paramter:
                tree: (complex structure) tree structure
                columns: (list) columns from header file
                idepth: (int) for indentation use
                root: (string) root at ith level of DT
    
        """
        for key, value in tree.iteritems():
            root_node = self.rootnode_str
            leaf_node = self.leafnode_str
            
            if type(value).__name__ == "str": 
                self.output_string += root_node.format(root, key)
                self.output_string += leaf_node.format(self.targetAtt, value)
            elif type(value).__name__ == "dict":
                if key in columns:
                    root = key
                elif root is not None:
                    self.output_string += root_node.format(root, key)
                    self.output_string += '\n'
                self.generateRules(value, columns, idepth, root)
                